#include "FOVController.hpp"

#include <cmath>

FOVController::FOVController(double angleOfViewInDegrees, double maxDistance): maxDistance_(maxDistance) {
    const double pi = std::acos(-1);
    angleOfView_ = angleOfViewInDegrees * (pi / 180); //transforming degrees to radians
}

bool FOVController::isInFOV(const Point& cone, const Point& vehiclePosition) const {
    return (xIsGood(cone, vehiclePosition) && yIsGood(cone, vehiclePosition));
}

bool FOVController::xIsGood(const Point& cone, const Point& vehiclePosition) const {
    return cone.x_ - vehiclePosition.x_ > 0 && cone.x_ - vehiclePosition.x_ < maxDistance_;
}

bool FOVController::yIsGood(const Point& cone, const Point& vehiclePosition) const {
    double xDistance = cone.x_ - vehiclePosition.x_;
    double yDistance = xDistance * std::tan(angleOfView_ / 2);
    return std::abs(cone.y_ - vehiclePosition.y_) < yDistance;
}
