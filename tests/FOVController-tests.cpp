#include "FOVController.hpp"

#include "gtest/gtest.h"
#include <tuple>

class FOVControllerTests : public ::testing::TestWithParam<std::tuple<Point, Point, bool>>
{
   protected:
    double maxDistance{30};
    double angleOfViewInDegrees{60};
};

TEST_P(FOVControllerTests, isInFOVTests)
{
    FOVController controller(angleOfViewInDegrees, maxDistance);
    Point conePosition;
    Point vehiclePosition;
    bool expectedResult{false};
    std::tie(conePosition, vehiclePosition, expectedResult) = GetParam();
    EXPECT_EQ(controller.isInFOV(conePosition, vehiclePosition), expectedResult);
}

INSTANTIATE_TEST_SUITE_P(CheckingManyConeAndVehiclePositions,
                         FOVControllerTests,
                         testing::Values(std::tuple<Point, Point, bool>{{10, 10}, {5, 10}, true},
                                         std::tuple<Point, Point, bool>{{10, 20}, {5, 10}, false}));
