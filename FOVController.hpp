#pragma once

#include "Point.hpp"

class FOVController {
    double maxDistance_{};
    double angleOfView_{}; //in radians
    
    bool xIsGood(const Point& cone, const Point& vehiclePosition) const;
    bool yIsGood(const Point& cone, const Point& vehiclePosition) const;
    public:
    FOVController() = default;
    FOVController(double angleOfViewInDegrees, double maxDistance);
    bool isInFOV(const Point& cone, const Point& vehiclePosition) const;
};
