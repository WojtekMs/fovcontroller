#include <cassert>
#include <iostream>

#include "FOVController.hpp"

int main() {
    FOVController controller(60, 30);
    Point cone1{0, 0};
    Point cone2{15, 15};
    Point cone3{20, 10};
    Point cone4{15, 5};
    Point cone5{40, 10};
    Point cone6{39, 10};
    Point vehiclePosition{10, 10};
    assert(controller.isInFOV(cone3, vehiclePosition) == true);
    assert(controller.isInFOV(cone1, vehiclePosition) == false);
    assert(controller.isInFOV(cone2, vehiclePosition) == false);
    assert(controller.isInFOV(cone4, vehiclePosition) == false);
    assert(controller.isInFOV(cone5, vehiclePosition) == false);
    assert(controller.isInFOV(cone6, vehiclePosition) == true);
}
